package com.example.home
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.util.Log
import android.widget.RemoteViews
import com.github.kittinunf.fuel.httpGet
import es.antonborri.home_widget.HomeWidgetBackgroundIntent
import es.antonborri.home_widget.HomeWidgetLaunchIntent
import es.antonborri.home_widget.HomeWidgetProvider
import com.github.kittinunf.result.Result
import org.json.JSONArray
import org.json.JSONTokener


class UserProvider : HomeWidgetProvider() {
    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray, widgetData: SharedPreferences) {
        appWidgetIds.forEach { widgetId ->
            val views = RemoteViews(context.packageName, R.layout.user_layout).apply {

                val pendingIntent = HomeWidgetLaunchIntent.getActivity(context,
                        MainActivity::class.java)
                setOnClickPendingIntent(R.id.user_widget_root, pendingIntent)

                val user = widgetData.getString("_user", "heyy")
                var userText = "this is $user"
                setTextViewText(R.id.tv_total_user_text, userText)


                val httpAsync = "https://jsonplaceholder.typicode.com/users"
                        .httpGet()
                        .responseString { request, response, result ->
                            when (result) {
                                is Result.Failure -> {
                                    val ex = result.getException()
                                    println(ex)
                                }
                                is Result.Success -> {
                                    val data = result.get()
                                    println(data.length)
                                    val jsonArray = JSONTokener(data).nextValue() as JSONArray

                                    Log.i("result", data.length.toString())
                                    Log.i("result", jsonArray.length().toString())

                                    setTextViewText(R.id.tv_total_user, jsonArray.length().toString())
                                }
                            }
                        }

                httpAsync.join()



                val backgroundIntent = HomeWidgetBackgroundIntent.getBroadcast(context,
                Uri.parse("myAppWidget://userWidget"))
                setOnClickPendingIntent(R.id.bt_update, backgroundIntent)
            }
            appWidgetManager.updateAppWidget(widgetId, views)
        }
    }
}